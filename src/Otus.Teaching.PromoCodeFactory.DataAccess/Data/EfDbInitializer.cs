﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            //_dataContext.Database.EnsureDeleted();

            if (_dataContext.Database.CanConnect())
            {
                _dataContext.Database.Migrate();
            }
            else
            {
                _dataContext.Database.Migrate();

                _dataContext.Database.EnsureCreated();

                _dataContext.AddRange(FakeDataFactory.Employees);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();

            }
        }
    }
}